# FriendsOnly

Chrome extension to change photos permissions on Facebook from public to friends only


How to use this extenstion?

Before installing browse to your photos on FB :

1. Go to https://www.facebook.com/
2. Click on your profile page
3. Click on photos
4. Click on Your Photos

Now install the chrome extenstion: 
1. Open a new tab and in the url paste:
chrome://extensions/
2. Switch to developer mode (on the top right)
3. load the code using "load unpacked"

Now refresh the FB page and let the extenstion start working!

